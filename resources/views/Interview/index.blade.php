@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="jumbotron">
                    <h1 class="display-3">Название опроса</h1>
                    <p class="lead">Краткое описание опроса.</p>
                    <hr class="my-4">
                    <p class="lead">
                        <a class="btn btn-success btn-lg" href="#" role="button">Пройти опрос</a>
                        <a class="btn btn-info btn-lg" href="#" role="button">Посмотреть статистику</a>
                    </p>
                </div>
            </div>
        </div>
    </div>
@endsection