@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col">
                {!! Form::open(['url' => 'admin/interview', 'method' => 'post']) !!}
                <div class="form-group">
                    <label for="text">Название опроса</label>
                    <input type="text" class="form-control" name="name">
                </div>
                <div id="app">
                    <div id="question" v-for="q in questions">
                        <div class="form-group">
                            <label for="text">Вопрос номер @{{ q.count }}</label>
                            <textarea class="form-control" id="exampleTextarea" rows="3" name="content" v-model="q.content"></textarea>
                        </div>
                        <div class="form-group">
                            <label>Тип вопроса</label>
                            <select class="form-control" v-model="q.type">
                                <option>По шкале</option>
                                <option>Свой ответ</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <button type="button" class="btn btn-info" @click.prevent="add()">Добавить вопрос</button>
                        <button type="submit" class="btn btn-info pull-right">Сохранить</button>
                    </div>

                    <pre>@{{ $data }}</pre>

                </div>

                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection