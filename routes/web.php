<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'InterviewController@index');
Route::resource('interview', 'InterviewController');

Route::group(['namespace' => 'Admin', 'prefix' => 'admin'], function () {
    Route::get('/', 'AdminController@index');
    Route::resource('interview', 'InterviewController');

});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
