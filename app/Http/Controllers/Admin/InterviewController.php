<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Interview;

class InterviewController extends Controller
{
    public function create()
    {
        return view('admin/interview.create');
    }

    public function store(Request $request)
    {

        $interview = new Interview;
        $interview->fill(['name' => $request->name]);
        $interview->save();

    }
}
